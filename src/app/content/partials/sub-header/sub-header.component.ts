import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sub-header',
  templateUrl: './sub-header.component.html',
  styleUrls: ['./sub-header.component.css']
})
export class SubHeaderComponent implements OnInit {
  lastLoginDate:string;
  policyNumber:string;

  constructor() { }

  ngOnInit(): void {
    this.policyNumber=localStorage.getItem('policynumber');
    this.lastLoginDate=localStorage.getItem('lastLoginDate');
  }

}
