import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../../core/services/login.service';
import { Router, ActivatedRoute } from '@angular/router';
import {IDocumentUrlModel} from '../../../core/interfaces/app.interface';
import {PolicyService} from '../../../core/services/policy.service';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-policy-documents',
  templateUrl: './policy-documents.component.html',
  styleUrls: ['./policy-documents.component.css']
})
export class PolicyDocumentsComponent implements OnInit {
documents: IDocumentUrlModel[] = [];
policyNumber:string;
effectiveDate:string;
expirationDate:String;
todayDate:String;
firstName:string;
lastName:string;
lastLoginDate:string;

  constructor(
    private loginService:LoginService,
    private router: Router,
    private route: ActivatedRoute,
    private policyService:PolicyService,
    public datepipe: DatePipe,
  ) { }

  ngOnInit(){
    this.policyNumber=localStorage.getItem('policynumber');
   // this.effectiveDate=localStorage.getItem('effectiveDate');
    //this.expirationDate=localStorage.getItem('expirationDate');
    this.firstName=localStorage.getItem('firstname');
    this.lastName=localStorage.getItem('lastname');
    this.lastLoginDate=localStorage.getItem('lastLoginDate');
    this.getDocuments(this.policyNumber);
    this.getPolicyNumberByDetails(this.policyNumber);
  }

  
  getPolicyNumberByDetails(policyNumber:string){
    this.policyService.PolicyDetailsByPolicyNumber(policyNumber).subscribe(data=>{
      if(data.data != null){
        if(data.status = 200){
          //  this.policyDetails=data.data;
           var effectiveDate = this.datepipe.transform(data.data["effectiveDate"], 'M/yy');
           data.data["effectiveDate"] = effectiveDate;
           this.effectiveDate=effectiveDate;
           var expirationDate = this.datepipe.transform(data.data["expirationDate"], 'M/yy');
           data.data["expirationDate"] = expirationDate;
           this.expirationDate=expirationDate;
           var paymentDate= this.datepipe.transform(data.data["installmentDueDate"], 'M/d/yy');
         }
      }
    },error=>{
      console.log();
    })
   }

  getDocuments(policyNumber:string){
    this.policyService.PolicyDocumentByPolicyNumber(policyNumber).subscribe(data=>{
     if(data.data != null){
      if(data.status = 200){
        var documentDate= this.datepipe.transform(data.data[0]["documentDate"], 'M/d/yy');
        data.data[0]["documentDate"]=documentDate;
        this.todayDate=documentDate;
        this.documents=data.data;
       }
     }
    },error=>{
      console.log();
    });
  }

  doLogout(){
    this.loginService.logOut().subscribe(data=>{
      localStorage.clear();
      this.router.navigate(["/login"]);
    },error=>{
       console.log();
    });
  }

  policyDetails() {
    this.router.navigate(['/policy-details/' + this.policyNumber]);
    return false;
  }

  payment(){
    this.router.navigate(['/payment-history/'+ this.policyNumber])
    return false;
  }

  policyDocuments(){
    this.router.navigate(['/policy-documents/'+ this.policyNumber])
    return false;
  }

}
