import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../../core/services/login.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-payment-success',
  templateUrl: './payment-success.component.html',
  styleUrls: ['./payment-success.component.css']
})
export class PaymentSuccessComponent implements OnInit {
  firstName:string;
  lastName:string;
  policyNumber:String;
  effectiveDate:string;
  expirationDate:String;
  installmentAmount:string;

  constructor(
    private loginService:LoginService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(){
    this.firstName=localStorage.getItem('firstname');
    this.lastName=localStorage.getItem('lastname');
    this.policyNumber=localStorage.getItem('policynumber');
    this.effectiveDate=localStorage.getItem('effectiveDate');
    this.expirationDate=localStorage.getItem('expirationDate');
    this.installmentAmount = localStorage.getItem('installmentAmount');
  }

  doLogout(){
    this.loginService.logOut().subscribe(data=>{
      localStorage.clear();
      this.router.navigate(["/login"]);
    },error=>{
       console.log();
    });
  }

  policyDetails() {
    this.router.navigate(['/policy-details/' + this.policyNumber]);
    return false;
  }

  payment(){
    this.router.navigate(['/payment-history/'+ this.policyNumber])
    return false;
  }

  policyDocuments(){
    this.router.navigate(['/policy-documents/'+ this.policyNumber])
    return false;
  }
}
