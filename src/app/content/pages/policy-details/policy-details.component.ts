import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from '../../../core/services/alert.service';
import {PolicyService} from '../../../core/services/policy.service';
import {LoginService} from '../../../core/services/login.service';
import { DatePipe } from '@angular/common';
import {IPolicyDetails,IAutoVehicles,IAutoDrivers} from '../../../core/interfaces/app.interface';
import { PaymentService } from 'src/app/core/services/payment.service';

@Component({
  selector: 'app-policy-details',
  templateUrl: './policy-details.component.html',
  styleUrls: ['./policy-details.component.css']
})
export class PolicyDetailsComponent implements OnInit {
  policyDetails: IPolicyDetails = <IPolicyDetails>{};
  autoVehicles: IAutoVehicles[] = [];
  autoDrivers: IAutoDrivers[] = [];
  policyNumber:string;
  effectiveDate:string;
  lastPaymentDate:String;
  expirationDate:string;
  paymentDate:string;
  firstName:string;
  lastName:string;
  day:string;
  lastLoginDate:string;
  isRecurring:boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private policyService:PolicyService,
    private loginService:LoginService,
    public datepipe: DatePipe,
    public paymentService:PaymentService,
    private alertservice:AlertService
    ) { }

  ngOnInit() {
    this.policyNumber = this.route.snapshot.params['policynumber'];
    if(this.policyNumber != null){
      this.getPolicyNumberByDetails(this.policyNumber);
    }
    this.firstName=localStorage.getItem('firstname');
    this.lastName=localStorage.getItem('lastname');
    this.lastLoginDate=localStorage.getItem('lastLoginDate');
  }

  getPolicyNumberByDetails(policyNumber:string){
   this.policyService.PolicyDetailsByPolicyNumber(policyNumber).subscribe(data=>{
     console.log(data)
     if(data.data != null){
       if(data.status = 200){
          this.policyDetails=data.data;
          var effectiveDate = this.datepipe.transform(data.data["effectiveDate"], 'M/yy');
          data.data["effectiveDate"] = effectiveDate;
          this.effectiveDate=effectiveDate;
          var expirationDate = this.datepipe.transform(data.data["expirationDate"], 'M/yy');
          data.data["expirationDate"] = expirationDate;
          this.expirationDate=expirationDate;
          var paymentDate= this.datepipe.transform(data.data["installmentDueDate"], 'M/d/yy');
          data.data["installmentDueDate"]=paymentDate;
          this.paymentDate=paymentDate;
          var day=this.datepipe.transform(data.data["installmentDueDate"], 'EEEE');
          data.data["installmentDueDate"]=day;
          this.day=day;
          var lastDate=this.datepipe.transform(data.data["lastPaymentDate"], 'M/d/yy');
          data.data["lastPaymentDate"]=lastDate;
          this.lastPaymentDate=lastDate;
          this.autoDrivers=data.data.autoDrivers;
          this.autoVehicles=data.data.autoVehicles;

          localStorage.setItem('installmentAmount',data.data.amount);
          localStorage.setItem('garageAddress1',data.data.garageAddress1);
          localStorage.setItem('garageAddress2',data.data.garageAddress2);
          localStorage.setItem('garageCity',data.data.garageCity);
          localStorage.setItem('garageState',data.data.garageState);
          localStorage.setItem('garageZip',data.data.garageZip);
          localStorage.setItem('isRecurring',data.data.isRecurring);
          this.isRecurring = data.data.isRecurring;
          console.log(this.isRecurring)
        }
     }
   },error=>{
     console.log();
   })
  }

  doLogout(){
    this.loginService.logOut().subscribe(data=>{
      localStorage.clear();
      this.router.navigate(["/login"]);
    },error=>{
       console.log();
    });
  }

  policyDetail() {
    this.router.navigate(['/policy-details/' + this.policyNumber]);
    return false;
  }

  payment(){
    this.router.navigate(['/payment-history/'+ this.policyNumber])
    return false;
  }

  policyDocuments(){
    this.router.navigate(['/policy-documents/'+ this.policyNumber])
    return false;
  }

  payNowRedirection(){
    this.router.navigate(["/payment-card-details"]);
    return false;
  }

  
}
