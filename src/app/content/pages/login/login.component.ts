import { Component, OnInit } from '@angular/core';
import { AbstractControl,FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import {LoginService} from '../../../core/services/login.service';
import { ILoginModel, ISignUp } from 'src/app/core/interfaces/app.interface';
import {AlertService} from '../../../core/services/alert.service';
import { DatePipe } from '@angular/common';
import {md5} from '../../../core/utilities/md5';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login:FormGroup;
  signUp:FormGroup;
  islogin: boolean = false;
  isSignUp: boolean = false;
  policyNumber:string;
  showModal: boolean;
  content: string;
  title: string;
  loginDetails:ILoginModel={policyNumber:null,password:null,email:null,loginPassword:null}
  signUpDetails:ISignUp={policyNumber:null,zipCode:null,birthDate:null}
  

  constructor(
    private formBuilder:FormBuilder,
    private router: Router,
    private loginservice: LoginService,
    private alertservice:AlertService,
    public datepipe: DatePipe,

  ) { }

  ngOnInit() {
    this.login=this.formBuilder.group({
      loginpolicynumber: [null, [Validators.required]],
      loginPassword: [null, [Validators.required]],
    });

    this.signUp=this.formBuilder.group({
      txtpolicynumber: [null, [Validators.required]],
      dob: [null, Validators.required],
    });
    
  }

  get loginpolicynumber() { return this.login.get('loginpolicynumber'); }
  get loginPassword() { return this.login.get('loginPassword'); }
  get txtpolicynumber() { return this.signUp.get('txtpolicynumber'); }
  get dob() { return this.signUp.get('dob'); }

  loginSubmit(){
    if(this.login.valid){
      let password = md5(this.loginDetails.loginPassword);
      this.loginDetails.password=password;
    this.loginservice.login(this.loginDetails).subscribe(data=>{
      if(data.data != null){
        if (data.status = 200){
          var effectiveDate = this.datepipe.transform(data.data.effectiveDate, 'M/yy');
          data.data.effectiveDate = effectiveDate;
          var expirationDate = this.datepipe.transform(data.data.expirationDate, 'M/yy');
          data.data.expirationDate = expirationDate;
          localStorage.setItem('policynumber', data.data.policyNumber);
          localStorage.setItem('firstname',data.data.firstName);
          localStorage.setItem('lastname',data.data.lastName);
          localStorage.setItem('effectiveDate',effectiveDate);
          localStorage.setItem('expirationDate',expirationDate);
          localStorage.setItem('producer',data.data.producer);

          if(data.data.lastLoginDate == null){
            data.data.lastLoginDate = this.datepipe.transform(new Date(),'MM/dd/yyyy hh:mm:ss');
          }
          localStorage.setItem('lastLoginDate',data.data.lastLoginDate);
          this.policyNumber=data.data.policyNumber;
          this.router.navigate(["/policy-documents/" + this.policyNumber]);
          // this.router.navigate(["/policy-details/" + this.policyNumber]);
        }
      }
    },error =>{
      this.alertservice.warning("Invalid user details !");
    })
    }  else{
      this.alertservice.warning('Please fill all required details');
      return false;
    }
  }

  show()
  {
    this.showModal = true;
  }

  hide()
  {
    window.location.reload();
  }

  checkRenewal(){
    if(this.signUp.valid){
      this.loginservice.checkRenewal(this.signUpDetails).subscribe(data=>{
        console.log(data);
        if(data.data != null){
          if(data.data.status == 200 && data.data.message == null){
            //  this.alertservice.warning('Please contact your agent to renew you policy.');
            
            //QA Redirection
            // window.location.href = "http://arrowhead-pos-customer-dev.azurewebsites.net/arrowhead/renewal?policy=" + this.signUpDetails.policyNumber;

            //Production Redirection
             window.location.href = "https://agentdirect.arrowheadauto.com/arrowhead/renewal?policy=" + this.signUpDetails.policyNumber;

          } else if(data.data.message != null && data.data.status == null){
            this.alertservice.warning(data.data.message);
            //  this.alertservice.warning('Please contact your agent to renew you policy.');
          } else{
            this.alertservice.warning('Please contact your agent to renew you policy.');
          }
        }else{
          this.alertservice.warning('Please contact your agent to renew you policy.');
        }  
      })
    }else{
      this.alertservice.warning('Please fill all required details');
      return false;
    }
  }
  
}
