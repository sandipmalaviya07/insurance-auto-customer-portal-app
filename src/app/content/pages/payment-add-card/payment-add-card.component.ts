import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../../core/services/login.service';
import { Router, ActivatedRoute } from '@angular/router';
import {ICCDetailModel} from 'src/app/core/interfaces/app.interface';
import {PaymentService} from '../../../core/services/payment.service';
import {AlertService} from '../../../core/services/alert.service';

@Component({
  selector: 'app-payment-add-card',
  templateUrl: './payment-add-card.component.html',
  styleUrls: ['./payment-add-card.component.css']
})
export class PaymentAddCardComponent implements OnInit {
  firstName:string;
  lastName:string;
  policyNumber:string;
  effectiveDate:string;
  expirationDate:String;
  message:string;
  installmentAmount:string;
  // ccDetailForm:ICCDetailModel={ccName:null,ccNumber:null,ccExpMonth:null,ccExpYear:null,ccCVV:null,policyNumber:null,isDefault:false,id:0,actionName:null,isRecurring:false,Email:null,Message:null}
  ccDetailForm: ICCDetailModel = <ICCDetailModel>{};
  
  constructor(
    private loginService:LoginService,
    private router: Router,
    private route: ActivatedRoute,
    private paymentService:PaymentService,
    private alertservice:AlertService
  ) { }

  ngOnInit(){
    this.firstName=localStorage.getItem('firstname');
    this.lastName=localStorage.getItem('lastname');
    this.policyNumber=localStorage.getItem('policynumber');
    this.effectiveDate=localStorage.getItem('effectiveDate');
    this.expirationDate=localStorage.getItem('expirationDate');
    this.installmentAmount = localStorage.getItem('installmentAmount');
  }

  doLogout(){
    this.loginService.logOut().subscribe(data=>{
      localStorage.clear();
      this.router.navigate(["/login"]);
    },error=>{
    });
  }

  policyDetails() {
    this.router.navigate(['/policy-details/' + this.policyNumber]);
    return false;
  }

  payment(){
    this.router.navigate(['/payment-history/'+ this.policyNumber])
    return false;
  }

  policyDocuments(){
    this.router.navigate(['/policy-documents/'+ this.policyNumber])
    return false;
  }

  addCCDetails(){
      this.ccDetailForm.policyNumber = this.policyNumber;

      console.log(this.ccDetailForm);
      debugger;

      this.paymentService.AddCCDetails(this.ccDetailForm).subscribe(data=>{
        if(data.status == 200){
          if(data.data != null){
            this.router.navigate(["/payment-card-details"]);
          }
        }else if(data.status == 208){
            this.message = data.data.message;
            console.log(this.message);
        }

        // if(data.data != null){
        //   if (data.status = 200){
        //     console.log(data)
        //     this.router.navigate(["/payment-card-details"]);
        //   } else if(data.status = 208){
        //     this.message = data.data.message;
        //     console.log(this.message);
        //   }
        // }
        },error =>{
          console.log(this.message);
          this.alertservice.warning("Invalid user details !");
      });
  }

   payNowRedirection(){
    this.router.navigate(["/payment-card-details"]);
    return false;
  }
}
