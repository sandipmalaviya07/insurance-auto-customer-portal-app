import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../../core/services/login.service';
import { Router, ActivatedRoute } from '@angular/router';
import {PaymentService} from '../../../core/services/payment.service';
import {AlertService} from '../../../core/services/alert.service';
import {IPolicyDetails, ICCDetailModel} from '../../../core/interfaces/app.interface';

@Component({
  selector: 'app-payment-card-details',
  templateUrl: './payment-card-details.component.html',
  styleUrls: ['./payment-card-details.component.css']
})
export class PaymentCardDetailsComponent implements OnInit {
  firstName:string;
  lastName:string;
  policyNumber:string;
  effectiveDate:string;
  expirationDate:String;
  cardList:ICCDetailModel[] = [];
  installmentAmount:string;
  policyDetailsModel: IPolicyDetails = <IPolicyDetails>{};
  DefaultCard:ICCDetailModel = <ICCDetailModel>{};

  constructor(
    private loginService:LoginService,
    private router: Router,
    private route: ActivatedRoute,
    private PaymentService: PaymentService,
    private alertservice:AlertService
  ) { }

  ngOnInit(){
    this.firstName=localStorage.getItem('firstname');
    this.lastName=localStorage.getItem('lastname');
    this.policyNumber=localStorage.getItem('policynumber');
    this.effectiveDate=localStorage.getItem('effectiveDate');
    this.expirationDate=localStorage.getItem('expirationDate');
    this.installmentAmount = localStorage.getItem('installmentAmount');
    console.log(localStorage.getItem('isRecurring'))
    if(localStorage.getItem('isRecurring') == "false"){
      
        this.DefaultCard.isRecurring = false;
    }else{
        this.DefaultCard.isRecurring = true;
    }
      
    this.getAllCards();
  }

  getAllCards(){
    this.PaymentService.GetAllCards(this.policyNumber).subscribe(data=>{
      if(data.data!= null){
        if(data.status=200){
          this.cardList = data.data.ccList;
          for(var val of data.data.ccList){
            if(val.isDefault == true){
              this.DefaultCard.ccName = val.ccName;
              this.DefaultCard.ccExpMonth = val.ccExpMonth;
              this.DefaultCard.ccExpYear = val.ccExpYear;
              this.DefaultCard.ccNumber = val.ccNumber;
              this.DefaultCard.isDefault = val.isDefault;
              // this.DefaultCard.ccDbId = val.id;
              this.DefaultCard.id = val.id;
            }            
          }
        }
      }
    },error =>{
      this.alertservice.warning("Invalid user details !");
      });
  }

  doLogout(){
    this.loginService.logOut().subscribe(data=>{
      localStorage.clear();
      this.router.navigate(["/login"]);
    },error=>{
       console.log();
    });
  }

  policyDetails() {
    this.router.navigate(['/policy-details/' + this.policyNumber]);
    return false;
  }

  payment(){
    this.router.navigate(['/payment-history/'+ this.policyNumber])
    return false;
  }

  policyDocuments(){
    this.router.navigate(['/policy-documents/'+ this.policyNumber])
    return false;
  }

   addNewCard(){
    this.router.navigate(['/payment-add-card'])
    return false;
  }

  payNow(){
    this.policyDetailsModel.PolicyNumber = this.policyNumber;
    this.policyDetailsModel.amount = parseFloat(this.installmentAmount);
    this.PaymentService.PayPolicyInstallment(this.policyDetailsModel).subscribe(data=>{
      if(data.status = 401){
        this.alertservice.warning(data.message);
      }
      if(data.data != null){
        if (data.status = 200){
          this.router.navigate(["/payment-success"]);
        }
      }
    },error =>{
      this.alertservice.warning("Payment Faild!");
    })
  }

  cardSelection(_row){
    this.DefaultCard=_row;
  }

  removeCard(){
    this.DefaultCard.actionName="Remove";
    this.DefaultCard.policyNumber=this.policyNumber;
    this.PaymentService.UpdateCard(this.DefaultCard).subscribe(data=>{
      if(data.status = 401){
        this.alertservice.warning(data.message);
      }
      if(data.data != null){
        if (data.status = 200){
          this.cardList = data.data.ccList;
          for(var val of data.data.ccList){
            if(val.isDefault == true){
              this.DefaultCard.ccName = val.ccName;
              this.DefaultCard.ccExpMonth = val.ccExpMonth;
              this.DefaultCard.ccExpYear = val.ccExpYear;
              this.DefaultCard.ccNumber = val.ccNumber;
              this.DefaultCard.isDefault = val.isDefault;
              // this.DefaultCard.ccDbId = val.id;
              this.DefaultCard.id = val.id;
            }            
          }
        }
      }
    },error =>{
      this.alertservice.warning("Payment Faild!");
    })
  }

  changeDefaultCard(){
    this.DefaultCard.actionName="Default";
    this.DefaultCard.policyNumber=this.policyNumber;
    console.log(this.DefaultCard);
    this.PaymentService.UpdateCard(this.DefaultCard).subscribe(data=>{
      if(data.status = 401){
        this.alertservice.warning(data.message);
      }
      if(data.data != null){
        if (data.status = 200){
          this.cardList = data.data.ccList;
          for(var val of data.data.ccList){
            if(val.isDefault == true){
              this.DefaultCard.ccName = val.ccName;
              this.DefaultCard.ccExpMonth = val.ccExpMonth;
              this.DefaultCard.ccExpYear = val.ccExpYear;
              this.DefaultCard.ccNumber = val.ccNumber;
              this.DefaultCard.isDefault = val.isDefault;
              // this.DefaultCard.ccDbId = val.id;
              this.DefaultCard.id = val.id;
            }            
          }
        }
      }
    },error =>{
      this.alertservice.warning("Payment Faild!");
    })
  }

  setupRecurringOption(){
    this.DefaultCard.policyNumber=this.policyNumber;
    this.PaymentService.changeRecurringOption(this.DefaultCard).subscribe(data=>{
      console.log(data)
      if(data.status == 200){
        if(data.data == "True"){
          this.DefaultCard.isRecurring = true;
          localStorage.setItem('isRecurring',"true");
        }else{
          this.DefaultCard.isRecurring = false;
          localStorage.setItem('isRecurring',"false");
        }
      }
    },error =>{
      this.alertservice.warning("Payment Faild!");
    })
  }
}
