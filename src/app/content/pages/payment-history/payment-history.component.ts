import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../../core/services/login.service';
import { Router, ActivatedRoute } from '@angular/router';
import {PaymentService} from '../../../core/services/payment.service';
import {PaymentHistoryModel,PaymentScheduleModel} from '../../../core/interfaces/app.interface';

@Component({
  selector: 'app-payment-history',
  templateUrl: './payment-history.component.html',
  styleUrls: ['./payment-history.component.css']
})
export class PaymentHistoryComponent implements OnInit {
  firstName:string;
  lastName:string;
  policyNumber:string;
  effectiveDate:string;
  expirationDate:string;
  lastLoginDate:string;
  installmentAmount:string;
  garageAddress1:string;
  garageAddress2:string;
  garageCity:string;
  garageState:string;
  garageZip:string;
  paymentHistoryList:PaymentHistoryModel[] = [];
  paymentScheduleList:PaymentScheduleModel[] = [];

  constructor(
    private loginService:LoginService,
    private router: Router,
    private route: ActivatedRoute,
    private PaymentService: PaymentService
  ) { }

  ngOnInit(){
    this.firstName=localStorage.getItem('firstname');
    this.lastName=localStorage.getItem('lastname');
    this.policyNumber=localStorage.getItem('policynumber');
    this.effectiveDate=localStorage.getItem('effectiveDate');
    this.expirationDate=localStorage.getItem('expirationDate');
    this.lastLoginDate=localStorage.getItem('lastLoginDate');
    this.installmentAmount = localStorage.getItem('installmentAmount');

    this.garageAddress1 = localStorage.getItem('garageAddress1');
    this.garageAddress2 = localStorage.getItem('garageAddress2');
    this.garageCity = localStorage.getItem('garageCity');
    this.garageState = localStorage.getItem('garageState');
    this.garageZip = localStorage.getItem('garageZip');

    this.getPaymentHistoryDetails();
  }

  
doLogout(){
  this.loginService.logOut().subscribe(data=>{
    localStorage.clear();
    this.router.navigate(["/login"]);
  },error=>{
     console.log();
  });
}

getPaymentHistoryDetails(){
  this.PaymentService.GetPaymentHistory(this.policyNumber).subscribe(data => {
    console.log(data)
    if(data.status == 200){
      if(data.data != null){
        this.paymentHistoryList = data.data.quotePayments;
        this.paymentScheduleList = data.data.paymentSchedule;
        console.log(this.paymentScheduleList);
      }
    }
  })
}

policyDetails() {
  this.router.navigate(['/policy-details/' + this.policyNumber]);
  return false;
}

payment(){
  this.router.navigate(['/payment-history/'+ this.policyNumber])
  return false;
}

policyDocuments(){
  this.router.navigate(['/policy-documents/'+ this.policyNumber])
  return false;
}

addPaymentCC(){
  this.router.navigate(['/payment-card-details'])
  return false;
}

payNowRedirection(){
    this.router.navigate(["/payment-card-details"]);
    return false;
  }

}
