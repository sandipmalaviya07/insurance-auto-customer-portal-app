import { Component, OnInit } from '@angular/core';
import {CustomerService} from '../../../core/services/customer.service'
import {AlertService} from '../../../core/services/alert.service'
import {AbstractControl, FormBuilder, Validators, FormGroup } from '@angular/forms';
import {ICustomerDetailModel} from '../../../core/interfaces/app.interface';
import {Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import {md5} from '../../../core/utilities/md5';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.css']
})
export class NewPasswordComponent implements OnInit {
  customerDetails: ICustomerDetailModel = <ICustomerDetailModel>{};
  IsCustomer:FormGroup;
  isCustomerDetail: boolean = false;
  policyNumber:string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private customerService:CustomerService,
    public datepipe: DatePipe,
    private formBuilder:FormBuilder,
    private alertservice:AlertService

  ) { }

  ngOnInit(){
    this.IsCustomer=this.formBuilder.group({
      txtpassword: [null, [Validators.required]],
      txtconfirmpassword: [null, [Validators.required,this.passwordConfirming]],
    })
    this.policyNumber=localStorage.getItem('policynumber');

    if (localStorage.getItem('alertmsg') != null && localStorage.getItem('alertmsg') != '') {
      this.alertservice.success(localStorage.getItem('alertmsg'));
    }
  }

  
  get txtpassword() { return this.IsCustomer.get('txtpassword'); }
  get txtconfirmpassword() { return this.IsCustomer.get('txtconfirmpassword'); }

  postCustomerDetail(){
    if(this.IsCustomer.valid){
      this.customerDetails.policyNumber=this.policyNumber;
      let password = md5(this.customerDetails.password);
      this.customerDetails.customerPassword=password;
      this.customerService.postCustomerDetail(this.customerDetails).subscribe(data=>{
        if (data.status == 400) {
          this.alertservice.warning(data.message);
          localStorage.setItem('alertmsg', data.message);
        }
        else {
          this.alertservice.success('Data saved successfully');
          localStorage.setItem('alertmsg', 'Data saved successfully');
          // this.router.navigate(["/login"]);
          this.router.navigate(["/policy-documents/" + this.policyNumber]);
        }
      })
    }else{
      this.alertservice.warning('Please fill all required details');
      return false;
    }
  }

  passwordConfirming(c: AbstractControl): any {
    if (!c.parent || !c) return;
    const pwd = c.parent.get('txtpassword');
    const cpwd = c.parent.get('txtconfirmpassword')
    if (!pwd || !cpwd) return;
    if (pwd.value !== cpwd.value) {
      return { invalid: true };
    }
  }

}
