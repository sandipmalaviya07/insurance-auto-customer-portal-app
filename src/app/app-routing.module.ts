import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { LoginComponent } from './content/pages/login/login.component';
import {SignUpComponent} from './content/pages/sign-up/sign-up.component';
import {PaymentSuccessComponent} from './content/pages/payment-success/payment-success.component';
import {PolicyDocumentsComponent} from './content/pages/policy-documents/policy-documents.component';
import {PolicyDetailsComponent} from './content/pages/policy-details/policy-details.component';
import {PolicyDetailsEditComponent} from './content/pages/policy-details-edit/policy-details-edit.component';
import {PaymentHistoryComponent} from './content/pages/payment-history/payment-history.component';
import {PaymentCardDetailsComponent} from './content/pages/payment-card-details/payment-card-details.component';
import {PaymentAddCardComponent} from './content/pages/payment-add-card/payment-add-card.component';
import {NewPasswordComponent} from './content/pages/new-password/new-password.component';

const routes: Routes = [

	{
		path: 'layout',
		loadChildren: 'app/content/layout/layout.module#LayoutModule',
	},
		{
			path: '',
		    component: LoginComponent
		},
	    {
		    path: 'login',
		     component: LoginComponent
		 },
		 {
			path: 'sign-up',
			component: SignUpComponent
		 },
		 {
			path: 'sign-up/:quote',
			component: SignUpComponent
		 },
		 {
			 path:'payment-success',
			 component:PaymentSuccessComponent
		 }, 
		 {
			path:'policy-documents/:policynumber',
			component:PolicyDocumentsComponent
		},
		{
			path:'policy-details/:policynumber',
			component:PolicyDetailsComponent
		},
		{
			path:'policy-details-edit',
			component:PolicyDetailsEditComponent
		},
		{
			path:'payment-history/:policynumber',
			component:PaymentHistoryComponent
		},
		{
			path:'payment-card-details',
			component:PaymentCardDetailsComponent
		},
		{
			path:'payment-add-card',
			component:PaymentAddCardComponent
		},
		{
			path:'password-confirmation',
			component:NewPasswordComponent
		},
		

	{
		path: '**',
		redirectTo: ''
	},

];

@NgModule({
	imports: [
		RouterModule.forRoot(routes, {
			preloadingStrategy: PreloadAllModules, useHash: true, onSameUrlNavigation: 'reload'
		},
		)
	],
	exports: [RouterModule]
})
export class AppRoutingModule { }
