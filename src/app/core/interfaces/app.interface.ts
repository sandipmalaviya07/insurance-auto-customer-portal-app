export interface ISignUp{
    policyNumber:string,
    zipCode :string,
    birthDate:Date
}

export interface IPolicyDetails{
  PolicyNumber:string,
  AgencyCode:string,
  Status:string,
  amount:number,
  paymentDueDate:Date,
  CancelDate:Date,
  EffectiveDate:Date,
  ExpirationDate:Date,
  firstName:string,
  lastName:string,
  Email:string,
  Phone:string,
  sixMonthPremium:number,
  outstandingBalance:number,
  lastReceivedAmount:number,
  lastReceivedAmtDate:Date,
  garageAddress1:string,
  GarageAddress2:string,
  garageCity:string,
  garageState:string,
  garageZip:string,
  MailingAddress1:string,
  MailingAddress2:string,
  MailingCity:string,
  MailingState:string,
  MailingZip:string,
  autoDrivers :IAutoDrivers[],
  autoVehicles :IAutoVehicles[]

    //  policyNumber:string,
    //  zipCode:string,
    //  customerId :number,
    //  address1:string,
    //  address2:string ,
    //  stateName:string,
    //  stateCode :string,
    //  effectiveDate :Date,
    //  expirationDate :Date,
    //  autoDrivers :IAutoDrivers[],
    //  autoVehicles :IAutoVehicles[]
    //  errorMessage:string,
    //  customerName:string,
    //  city:string,
    //  installmentDueDate:Date,
    //  monthlyPremium:number,
    //  totalPremium:number,
    //  lastPaymentDate:Date,
    //  downPaymentAmount:number
}

export interface IAutoVehicles{
  VehicleId:number,
  IsActive:boolean,
  vin:string,
  year:number,
  model:string,
  make:string,
  lhName:string,
  LhName2:string,
  LhAddress1:string,
  LhAddress2:string,
  LhCity:string,
  LhState:string
  
  
        //  id:number,
        //  quoteId:number,
        // lienHolderId:number,
        // ownerTypeId:number,
        // carUsedforId:number,
        // frequencyMileId :number,
        // estimatedMileage:number,
        // address1:string,
        // address2:string,
        // zipCode:string,
        // stateId:number,
        // city:string,
        //  vehicleOrderId:number,
        //  year :string,
        //  make :string,
        //  vin :string,
        //  model:string,
        //  bodyType :string,
        //  comp :number,
        // coll :number,
        //  towing :number,
        //  createdById:number,
        //  createdDate :Date,
        //  modifiedDate:Date,
        //  modifiedById :number,
        //  lienHoldersAddress:string
        //  garagingAddress1:string,
        //  garagingAddress2:string,
        //  garagingZipCode:string,
        //  garagingCity:string,
        // garagingFkstateCode:string,
        //  lienHolderName:string
        //  purchaseDate:Date,
        // isMonitoryDevice:boolean

}

export interface IAutoDrivers{
  driverId:number,
  name:string,
  DOB:Date,
  IsActive:boolean,
  MaritalStat:string,
  LicenseNo:string,
  DefensiveDisc:boolean,
  Gender:string,
  Sr22:boolean,
  Sr22A:boolean,
  WorkLoss:boolean,
  Occupation:string


//    id :number,
//    quoteId :number,
//    stateId:number,
//    birthDate:Date,
//  firstName:string,
//     lastName: string,
//      licenseNo :string,
//      gender :string,
//      socialSecurity :string,
//       driverOrderId :number,
//       maritalStatusId :number,
//       creditScoreId :number,
//       driverEducationId :number,
//       homeTypeId :number,
//       relationCdId :number,
//       isExcluded:boolean, 
//       isSr22:boolean ,
//       createdDate :Date,
//       createdById :number,
//       employer:string, 
//       employerYears :number,
//       milesToWork :number,
//       YearsLicensed :number,
//       licenseStatus :number,
//       dateFirstLicense :Date,
//       isDefensiveDriverCourse :boolean,
//       drivertrainingDate:Date, 
//       isSr22a :boolean
//       StateSr22id :number,
//       stateSr22aid :number
//      sr22reason:string, 
//       isWorkLossBenefit :boolean,
//       isSeniorDriver:boolean 
//       driverOccupationId :number,
//       violationPoints :number,
//       isNoHit:boolean 
//       militaryTypeId :number,
//       warehouseTypeId :number,
//       relationName:string,
//       isGoodStudent:boolean 
}

export interface ILoginModel{
  policyNumber:string,
  password:string,
  email:string,
  loginPassword:string,
}

export interface IDocumentUrlModel{
  documentName:string,
  documentUrl:string,
  documentId:number,
  documentDate:Date,
  year:number,
  model:string,
  vin:string,
  make:string,
}

export interface ICustomerDetailModel{
  policyNumber:string,
  confirmPassword:string,
  password:string,
  customerPassword:string
}

export interface ICCDetailModel{
  ccName:string,
  ccNumber:string,
  ccExpMonth:number,
  ccExpYear:number,
  ccCVV:string,
  policyNumber:string,
  isDefault:boolean,
  id:number,
  actionName:string,
  isRecurring:boolean,
  Email:string,
  Message:string
}

export interface PaymentHistoryModel{
  paidAmount:number,
  paymentDate:Date,
  paymentMethodName:string,
  paymentStatusName:string
}

export interface PaymentScheduleModel{
  paymentAmount:number,
  paidOrNot:boolean,
  dueDate:Date,
  paymentNo:string

  // dueAmount:number,
  // paidOrNot:boolean,
  // paymentDate:Date,
  // paymentNumber:string
}