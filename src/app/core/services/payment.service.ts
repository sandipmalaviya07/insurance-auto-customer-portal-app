import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/observable';
import {ErrorObservable} from 'rxjs/observable/ErrorObservable';
import {catchError} from 'rxjs/operators';
import {AppConstants} from '../utilities/app.constant';
import { Injectable } from '@angular/core';
import { IPolicyDetails } from '../interfaces/app.interface';
import {ICCDetailModel}  from '../interfaces/app.interface';

@Injectable({
    providedIn: 'root'
  })

  export class PaymentService{
    constructor(
        private http: HttpClient,
    ) { }

    PayPolicyInstallment(PolicyDetail: IPolicyDetails):Observable<any>{
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
              })
        };
        return this.http.post(AppConstants.apiBaseURL + 'v1/payInstallment',PolicyDetail,httpOptions)
        .pipe(catchError(this.handleError));
    }

    AddCCDetails(CCDetails: ICCDetailModel):Observable<any>{
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
              })
        };
        console.log("ccDetails : " + CCDetails.policyNumber);
        return this.http.post(AppConstants.apiBaseURL + 'v1/saveCC',CCDetails, httpOptions)
        .pipe(catchError(this.handleError));
    }

    GetAllCards(policyNumber:string):Observable<any>{
        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type': 'application/json'
            })
          };
          return this.http.get(AppConstants.apiBaseURL + 'v1/getAllCards/' + policyNumber,httpOptions)
            .pipe(catchError(this.handleError));
    }

    UpdateCard(CCDetails: ICCDetailModel):Observable<any>{
      const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
              })
        };
        return this.http.post(AppConstants.apiBaseURL + 'v1/updateCCDetails',CCDetails, httpOptions)
        .pipe(catchError(this.handleError));
    }

    changeRecurringOption(CCDetails:ICCDetailModel):Observable<any>{
      const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
              })
        };
        return this.http.post(AppConstants.apiBaseURL + 'v1/recurringSetup',CCDetails, httpOptions)
        .pipe(catchError(this.handleError));
    }

 GetPaymentHistory(policyNumber:string):Observable<any>{
        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type': 'application/json'
            })
          };
          return this.http.get(AppConstants.apiBaseURL + 'v1/getPaymentHistory/' + policyNumber,httpOptions)
            .pipe(catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse) {
        if (error.status == 401) {
            return new ErrorObservable(error.error.message);
        }
        console.log('Status:' + error.status + ',' + error.error.message);
        return new ErrorObservable(error.error.message);
    };
  }