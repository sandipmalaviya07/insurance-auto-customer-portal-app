export class AppConstants{
    
    /******************** Start Local URL ***************************/
    // public static get apiBaseURL(): string { return 'http://arrowhead-customerportalapi-dev.azurewebsites.net/api/' } 
    // public static get apiBaseURL(): string { return 'http://localhost:53528//api/' }
    /******************** Start Local URL ***************************/

    /******************** Production API URL ***************************/
     public static get apiBaseURL(): string { return 'http://arrowhead-customerportalapi-prod.azurewebsites.net/api/' } 
    /******************** Production API URL ***************************/
}